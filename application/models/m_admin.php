<?php
Class M_admin extends CI_Model
{

	function sesiku(){
		$this->session->set_flashdata('alert', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Login</div>');
		if($this->session->statusadmin != "online"){
			redirect('admin/login','refresh');
		}
	}

	// Count Dashboard Port
	function cport(){
		return $this->db->query('SELECT COUNT(*) as jumlah FROM port;')->result();
	}

	// Count Dashboard Destination
	function cdestination(){
		return $this->db->query('SELECT COUNT(*) as jumlah FROM destination;')->result();
	}

	// Count Dashboard Vessel
	function cvessel(){
		return $this->db->query('SELECT COUNT(*) as jumlah FROM vessel;')->result();
	}

	// Count Dashboard Rute
	function crute(){
		return $this->db->query('SELECT COUNT(*) as jumlah FROM rute;')->result();
	}


	// View User
	function tampil_user(){
		return $this->db->get('users')->result();	
	}

	// View Port
	function tampil_port($port_id=''){
		if($port_id!=''){
			$where = ' AND A.port_id = "'.$port_id.'"';
		} else {
			$where = '';
		}
		$query = $this->db->query('SELECT A.port_id,A.port_name,A.port_iso, B.destination_name,B.destination_id as idkota FROM port A,destination B WHERE A.destination_id=B.destination_id' . $where);
		return $query;
		// return $this->db->get('airport')->result();	
	}

	function get_port($port_id=''){
		if($port_id!=''){
			$where = ' AND destination_id = "'.$port_id.'"';
		} else {
			$where = '';
		}
		$query = $this->db->query('SELECT * FROM port WHERE 1=1 ' . $where);
		return $query;
	}

	// View Rute
	function tampil_rute($rute_id=''){
		if($rute_id!=''){
			$where = ' AND A.port_id = "'.$port_id.'"';
		} else {
			$where = '';
		}
		$query= $this->db->query('SELECT A.*,B.vessel_name as vessel FROM rute A, vessel B WHERE A.rute_id=B.vessel_id '.$where.' ORDER BY date_inserted DESC');
    	return $query->result();
		
    }

	// View Vessel
	function tampil_vessel(){
		return $this->db->get('vessel')->result();
	}

	function tampil_reservation(){
		$query = $this->db->query('SELECT R.*,C.id_users,C.name,C.noid,JR.rute_from,JR.rute_to,(select name from airport where JR.rute_from=airport.id) AS mktndol,(select iso from airport where JR.rute_from=airport.id) AS codemkt,(select iso from airport where JR.rute_to=airport.id) AS codebli,(select name from airport where JR.rute_to=airport.id) AS blindol FROM reservation R, customer C JOIN rute JR JOIN airport JA WHERE R.customer_id=C.id AND R.rute_id=JR.id AND JR.rute_from=JA.id ORDER BY reservation_date DESC');
		return $query->result();
	}

	// View Destination
	function tampil_destination(){
		return $this->db->get('destination')->result();
	}

	// View Agent
	function tampil_agent(){
		return $this->db->get('agent')->result();
	}



	// Add Destination
	function add_destination($data){
    $this->db->insert('destination', $data);
	}

	// Add Vessel
	function add_vessel($data){
    $this->db->insert('vessel', $data);
	}

	// Add Port
	function add_port($data){
    $this->db->insert('port', $data);
	}

	// Add Rute
	function add_rute($data,$table){
		$this->db->insert($table,$data);
	}

	// Add Agent
	function add_agent($data){
    $this->db->insert('agent', $data);
	}



	// Edit Destination
	function edit_destination($destination,$where){
		return $this->db->get_where($destination,$where);
	}

	// Edit User
	function edit_user($users,$user_id){
		return $this->db->get_where($users,$where);
	}

	// Edit Port
	function edit_port($table,$port_id){
		$query = $this->db->query('SELECT A.*,D.destination_name FROM port A JOIN destination D WHERE A.port_id='.$port_id.' AND D.destination_id=A.destination_id');
		return $query;
	}

	// Edit Vessel
	function edit_vessel($table,$vessel_id){
		$this->db->where('vessel_id', $vessel_id);
		return $this->db->get($table,$vessel_id);
	}

	// Edit Agent
	function edit_agent($agent,$where){
		return $this->db->get_where($agent,$where);
	}



	// Update Destination
	function update_destination($destination_id,$data){
		$this->db->where('destination_id', $destination_id);
		$this->db->update('destination', $data);
	}

	// Update User
	function update_user($user_id,$data){
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}

	// Update Port
	function update_port($port_id,$data){
		$this->db->where('port_id', $port_id);
		$this->db->update('port', $data);
	}

	// Update Rute
	function update_rute($rute_id,$data){
		$this->db->where('rute_id', $rute_id);
		$this->db->update('rute', $data);
	}

	// Update Vessel
	function update_vessel($vessel_id,$data){
		$this->db->where('vessel_id', $vessel_id);
		$this->db->update('vessel', $data);
	}

	// Update Agent
	function update_agent($agent_id,$data){
		$this->db->where('agent_id', $agent_id);
		$this->db->update('users', $data);
	}



	// Delete Destination
	function hapus_destination($destination_id){
		$this->db->where('destination_id', $destination_id);
		$this->db->delete('destination');
	}

	//  Delete User
	function hapus_user($user_id){
		$this->db->where('user_id', $user_id);
		$this->db->delete('users');
	}

	// Delete Rute
	function hapus_rute($rute_id){
		$this->db->where('rute_id', $rute_id);
		$this->db->delete('rute');
	}

	// Delete Vessel
	function hapus_vessel($vessel_id){
		$this->db->where('vessel_id', $vessel_id);
		$this->db->delete('vessel');
	}

	// Delete Port
	function hapus_port($port_id){
		$this->db->where('port_id', $port_id);
		$this->db->delete('port');
	}

	// Delete Agent
	function hapus_agent($agent_id){
		$this->db->where('agent_id', $agent_id);
		$this->db->delete('agent');
	}

}
?>