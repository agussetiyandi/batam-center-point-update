<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Port</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Manage Data</h1>
				<div class="row">
				</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Port</h3>
							</div>
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th style="text-align: center" width="5%">No</th>
											<th style="text-align: center">Port Name</th>
											<th style="text-align: center">Port Code</th>
											<th style="text-align: center">Destination</th>
											<th style="text-align: center" width="15%"></th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1; ?>
										<?php foreach ($port as $data){?>
										<tr>
											<td style="text-align: center"><?php  echo $no++; ?></td>
											<td><?php echo $data->port_name ?></td>
											<td style="text-align: center"><?php echo $data->port_iso ?></td>
											<td><?php echo $data->destination_name ?></td>
											<td style="text-align: center">
												<a type="button" href="<?php echo base_url('admin/port/edit/'.$data->port_id) ?>"
													class="btn btn-default btn-sm"><span class="fa fa-pencil"></span></a>
												<a type="button" href="<?php echo base_url('admin/port/del/'.$data->port_id) ?>"
													onclick="return confirm('Delete <?=$data->port_name ?> ?')"
													class="btn btn-default btn-sm"><span class="fa fa-trash"></span></a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
		</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>