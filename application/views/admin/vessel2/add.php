<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Create Vessel</title>
  <?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php $this->load->view('admin/common/header'); ?>
    <?php $this->load->view('admin/common/menu'); ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>Create Data</h1>
        <form action="<?php echo base_url(). 'admin/vessel/add_vessel'; ?>" method="post">
      </section>
      <section class="content">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Vessel</h3>
          </div>

          <div class="box-body">
            <div class="form-group">
              <label>Vessel Code</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-qrcode"></i>
                </div>
                <input name="agent_code" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Vessel Name</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-book"></i>
                </div>
                <input name="agent_name" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Seat</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-compass"></i>
                </div>
                <input name="agent_address" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Baggage</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-phone"></i>
                </div>
                <input name="agent_phone" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Country</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-flag"></i>
                </div>
                <input name="agent_country" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Flag</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-envelope"></i>
                </div>
                <input name="agent_email" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Info</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-address-book"></i>
                </div>
                <input name="agent_contact" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Owner Code</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-info-circle"></i>
                </div>
                <input name="agent_info" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <div class="form-group">
              <label>Agent Code</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-info-circle"></i>
                </div>
                <input name="agent_info" type="text" class="form-control" placeholder=". . ." required />
              </div>
            </div>
            <input type="submit" class="btn btn-primary" />
          </div>


            
      </section>
      </form>
    </div>
    <div class="control-sidebar-bg"></div>
  </div>
  <?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>