<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
			<section class="content">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Destination</h3>
						<?php foreach ($destination as $data){?>
						<form action="<?php echo base_url(). 'admin/destinations/update/'.$data->destination_id; ?>" method="post"
							class="form-horizontal">
					</div>
					<div class="box-body">
						<div class="form-group">
							<label>Destinationn</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-ship"></i>
								</div>
								<input name="destination_name" type="text" class="form-control" placeholder="Destination"
									value="<?= $data->destination_name ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Code Destination</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-qrcode"></i>
								</div>
								<input name="destination_iso" type="text" class="form-control" value="<?= $data->destination_iso ?>"
									placeholder="Code" required />
							</div>
							<?php } ?>
						</div>
						<div class="form-group">
							<label>Flag</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-image"></i>
								</div>
								<input name="destination_img" type="file" class="form-control" />
							</div>
						</div>
						<input type="submit" class="btn btn-primary" />
					</div>
					</form>
				</div>
			</section>


		</div>
		<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>