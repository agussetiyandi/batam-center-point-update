<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Agent</title>
  <?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php $this->load->view('admin/common/header'); ?>
    <?php $this->load->view('admin/common/menu'); ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>Agent</h1>

        <div class="row"> </div>
      </section>
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="float-right d-none d-md-block">
                <div id="add-button">
                  <a href="<?= base_url('admin/agent/add') ?>" class="btn btn-success mdi mdi-plus mr-2"> Create
                    Agent</a>
                </div>
              </div>

              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th align="center">No</th>
                      <th align="center">Agent Code</th>
                      <th align="center">Agent Name</th>
                      <th align="center">Address</th>
                      <th align="center">Phone</th>
                      <th align="center">Country</th>
                      <th align="center" width="15%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($agent as $data){?>
                    <tr>
                      <td align="center"><?php  echo $no++; ?></td>
                      <td align="center"><?php echo $data->agent_code ?></td>
                      <td><?php echo $data->agent_name ?></td>
                      <td><?php echo $data->agent_address ?></td>
                      <td><?php echo $data->agent_phone ?></td>
                      <td align="center"><?php echo $data->agent_country ?></td>
                      <td width="9%">
                        <a type="button" href="<?php echo base_url('admin/agent/edit/'.$data->agent_id) ?>"
                          class="btn btn-default btn-sm"><span class="fa fa-pencil"></span></a>
                        <a type="button" href="<?php echo base_url('admin/agent/del/'.$data->agent_id) ?>"
                          onclick="return confirm('Delete <?=$data->agent_name ?> ?')"
                          class="btn btn-default btn-sm"><span class="fa fa-trash"></span></a>


                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  </section>
  </div>
  <div class="control-sidebar-bg"></div>
  </div>
  <?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>