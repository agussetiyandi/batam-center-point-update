<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Vessel</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Edit Data Vessel</h1>
				<form action="<?php echo base_url(). 'admin/vessel/update'; ?>" method="post" enctype="multipart/form-data">
					<?php foreach ($vessel as $t) {?>
				</section>
				<section class="content">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Vessel</h3>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Logo</label>
								<div class="input-group">
									<img src="<?=base_url() ?>assets/images/vessel/<?php echo $t->vessel_img ?>" width="90px" height="80px">
								</div>
							</div>
							<div class="form-group">
								<label>Logo</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-image"></i>
									</div>
									<input name="vessel_img" type="file" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label>Vessel Name</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-ship"></i>
									</div>
									<input name="vessel_name" type="text" class="form-control" placeholder="Vessel Name" value="<?php echo $t->vessel_name ?>" required/>
								</div>
							</div>
							<div class="form-group">
								<label>Vessel Code</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-qrcode"></i>
									</div>
									<input name="vessel_code" type="text" class="form-control" value="<?php echo $t->vessel_code ?>"  placeholder="Vessel Code" required/>
								</div>
							</div>
							<div class="form-group">
								<label>Vessel Info</label>
								<textarea name="vessel_info" class="form-control" rows="3"><?php echo $t->vessel_info ?></textarea>
							</div>
							<div class="form-group">
								<label>Seat Qty</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-at"></i>
									</div>
									<input name="vessel_seat_qty" type="text" class="form-control" value="<?php echo $t->vessel_seat_qty ?>"  placeholder="Seat Qty" required/>
									<input name="vessel_id" type="hidden"  value="<?php echo $t->vessel_id ?>"/>
								</div>
							</div>
							<input type="submit" class="btn btn-primary" />
						</div>
					</div>
				</section>
				<?php } ?>
			</form> 
		</div>
		<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>
</html>