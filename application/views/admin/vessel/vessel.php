<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Vessel</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Manage Data</h1>
				<div class="row">
				</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Vessel</h3>
							</div>
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th style="text-align: center" width="5%">No</th>
											<th style="text-align: center" width="10%">Logo</th>
											<th style="text-align: center" width="10%">Code</th>
											<th style="text-align: center" width="20%">Vessel Name</th>
											<th style="text-align: center">Seat Qty</th>
											<th style="text-align: center">Vessel Info</th>
											<th style="text-align: center">Flag</th>
											<th style="text-align: center">Agent Code</th>
											<th style="text-align: center" width="15%"></th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1; ?>
										</tr><?php foreach ($vessel as $data){?>
										<tr>
											<td style="text-align: center"><?php  echo $no++; ?></td>
											<td style="text-align: center">
												<img src="<?php echo base_url(); ?>assets/images/vessel/<?php echo $data->vessel_img?>"
													width="70px" height="60px">
											</td>
											<td style="text-align: center"><?php echo $data->vessel_code ?></td>
											<td><?php echo $data->vessel_name ?></td>											
											<td style="text-align: center"><?php echo $data->vessel_seat_qty ?></td>
											<td><?php echo $data->vessel_info ?></td>
											<td style="text-align: center"><?php echo $data->vessel_flag ?></td>
											<td style="text-align: center"><?php echo $data->agent_code ?></td>
											<td style="text-align: center">
												<a type="button" href="<?=base_url()  ?>admin/vessel/edit/<?php echo $data->vessel_id?>"
													class="btn btn-default btn-sm"><span class="fa fa-pencil"></span></a>
												<a type="button" href="<?=base_url()  ?>admin/vessel/del/<?php echo $data->vessel_id?>"
													onclick="return confirm('Delete <?= $data->vessel_name ?>?')"
													class="btn btn-default btn-sm"><span class="fa fa-trash"></span></a></td>
										</tr><?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>