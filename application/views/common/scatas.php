<!-- Meta
	============================================= -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="author" content="IT Geeks">
	<!-- description -->
	<meta name="description" content="">
	<!-- keywords -->
	<meta name="keywords" content="">

	<!-- Stylesheets
	============================================= -->
	<link href="<?php echo base_url(); ?>gudang/css/css-assets.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>gudang/css/style.css" rel="stylesheet">

	<!-- Favicon
	============================================= -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>gudang/images/general-elements/favicon/favicon.png">
	<link rel="apple-touch-icon"
		href="<?php echo base_url(); ?>gudang/images/general-elements/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72"
		href="<?php echo base_url(); ?>gudang/images/general-elements/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114"
		href="<?php echo base_url(); ?>gudang/images/general-elements/favicon/apple-touch-icon-114x114.png">

	<!-- Title
	============================================= -->
	<title>Batam Center Point - Online Ticket Booking</title>
