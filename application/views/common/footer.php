<footer id="footer">

			<div id="footer-bar-1" class="footer-bar">

				<div class="footer-bar-wrap">

					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="fb-row">

									<div class="row">
										<div class="col-md-3">

											<img class="mb-30 logo-footer" src="<?php echo base_url(); ?>gudang/images/logo-bcp-header.png" alt="">
											<p>
												Consectetur adipisicing elit, sed do eiusmo incididunt ut labore et dolore magna aliqua. Ut enim
												minim
												veniam, Lorem ipsum dolor sit amet, sed do eiusmo incididunt.
											</p>

										</div><!-- .col-md-3 end -->
										<div class="col-md-3 mt-md-50">
											<h4>
												Gallery
											</h4>
											<ul class="list-gallery-thumbs list-lightbox-gallery">
												<li><a href="images/files/instagram-feed/img-1.jpg" class="img-bg lightbox-gallery"><img
															src="<?php echo base_url(); ?>gudang/images/files/instagram-feed/img-1.jpg" alt=""></a></li>
												<li><a href="images/files/instagram-feed/img-2.jpg" class="img-bg lightbox-gallery"><img
															src="<?php echo base_url(); ?>gudang/images/files/instagram-feed/img-2.jpg" alt=""></a></li>
												<li><a href="images/files/instagram-feed/img-3.jpg" class="img-bg lightbox-gallery"><img
															src="<?php echo base_url(); ?>gudang/images/files/instagram-feed/img-3.jpg" alt=""></a></li>
												<li><a href="images/files/instagram-feed/img-4.jpg" class="img-bg lightbox-gallery"><img
															src="<?php echo base_url(); ?>gudang/images/files/instagram-feed/img-4.jpg" alt=""></a></li>
												<li><a href="images/files/instagram-feed/img-5.jpg" class="img-bg lightbox-gallery"><img
															src="<?php echo base_url(); ?>gudang/images/files/instagram-feed/img-5.jpg" alt=""></a></li>
												<li><a href="images/files/instagram-feed/img-6.jpg" class="img-bg lightbox-gallery"><img
															src="<?php echo base_url(); ?>gudang/images/files/instagram-feed/img-6.jpg" alt=""></a></li>
											</ul><!-- .list-gallery-thumbs -->
										</div><!-- .col-md-3 end -->
										<div class="col-md-3 mt-md-50">
											<h4>
												Contact Us
											</h4>
											<ul class="list-contact-info">
												<li>
													<i class="fas fa-map-marker-alt"></i>
													<div class="info-content">
														<span>International Ferry Terminal Batam Center, Batam, Indonesia</span>
													</div><!-- .info-content end -->
												</li>
												<li>
													<i class="fas fa-phone-alt"></i>
													<div class="info-content">
														<span>(+62) 778 - xxx - xxx</span>
													</div><!-- .info-content end -->
												</li>
												<li>
													<i class="fas fa-envelope"></i>
													<div class="info-content">
														<span>ticket@batamcenterpoint.com</span>
													</div><!-- .info-content end -->
												</li>
											</ul><!-- .list-contact-info end -->
										</div><!-- .col-md-3 end -->
										<div class="col-md-3 mt-md-50">

											<h4>
												Stay In Touch
											</h4>
											<form class="form-newsletter-register form-inline form-h-50">
												<div class="form-group">
													<div class="box-field">
														<input type="text" name="email" class="form-control" placeholder="Enter Your Email Address">
														<button type="submit" class="form-control icon"><i
																class="fas fa-long-arrow-alt-up"></i></button>
													</div><!-- .box-field end -->
												</div><!-- .form-group end -->
												<span class="form-note">Enter your email address for promotions, news and updates.</span>
											</form><!-- .form-newsletter-register end -->
											<ul class="social-icons x5 dark hover-colorful icon-only mt-10">
												<li><a class="si-youtube" href="javascript:;"><i class="fa fa-youtube"></i><i
															class="fa fa-facebook"></i></a></li>
												<li><a class="si-twitter" href="javascript:;"><i class="fa fa-twitter"></i><i
															class="fa fa-twitter"></i></a>
												</li>
												<li><a class="si-facebook" href="javascript:;"><i class="fa fa-facebook"></i><i
															class="fa fa-twitter"></i></a></li>
												<li><a class="si-instagramorange" href="javascript:;"><i class="fa fa-instagram"></i><i
															class="fa fa-instagram"></i></a></li>
											</ul><!-- .social-icons end -->

										</div><!-- .col-md-3 end -->
									</div><!-- .row end -->

								</div><!-- .fb-row end -->

							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .footer-bar-wrap -->

			</div><!-- #footer-bar-1 end -->

			<div id="footer-bar-2" class="footer-bar">

				<div class="footer-bar-wrap">

					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="fb-row">
									<div class="copyrights-message">
										<span>© 2020</span> <a href="#" target="_blank">Batam Center Point. </a>
										<span>All Right Reserved</span>
									</div><!-- .copyrights-message end -->

								</div><!-- .fb-row end -->

							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .footer-bar-wrap -->

			</div><!-- #footer-bar-2 end -->

		</footer><!-- #footer end -->