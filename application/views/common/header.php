<header id="header">

			<div id="header-bar-1" class="header-bar">

				<div class="header-bar-wrap">

					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="hb-content">
									<div class="position-left">
										<ul class="list-info">
											<li>
												<ul class="social-icons x4 grey hover-white icon-only">
													<li><a class="si-youtube" href="javascript:;"><i class="fa fa-youtube"></i><i
																class="fa fa-youtube"></i></a></li>
													<li><a class="si-twitter" href="javascript:;"><i class="fa fa-twitter"></i><i
																class="fa fa-twitter"></i></a>
													</li>
													<li><a class="si-facebook" href="javascript:;"><i class="fa fa-facebook"></i><i
																class="fa fa-facebook"></i></a></li>
													<li><a class="si-instagramorange" href="javascript:;"><i class="fa fa-instagram"></i><i
																class="fa fa-instagram"></i></a></li>
												</ul><!-- .social-icons end -->
											</li>
											<li>
												<a href="javascript:;">ticket@batamcenterpoint.com</a>
											</li>
										</ul><!-- .list-info end -->
									</div><!-- .position-left end -->
									<div class="position-right">
										<ul class="list-info">
											<li>(+62) 778 - xxx - xxx</li>
											<li><a class="popup-btn-login" href="javascript:;">Login</a></li>
											<li><a class="popup-btn-register" href="javascript:;">Sign Up</a></li>
										</ul><!-- .list-info end -->
									</div><!-- .position-right end -->
								</div><!-- .hb-content end -->

							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .header-bar-wrap -->

			</div><!-- #header-bar-1 end -->

			<div id="header-bar-2" class="header-bar sticky">

				<div class="header-bar-wrap">

					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="hb-content">
									<a class="logo logo-header" href="index.html">
										<img src="<?php echo base_url(); ?>gudang/images/logo-bcp-header.png"
											data-logo-alt="<?php echo base_url(); ?>gudang/images/files/logo-header-alt.png" alt="">
										<h3><span class="colored">IT Geeks</span></h3>
										<span>Web Services</span>
									</a><!-- .logo end -->
									<ul id="menu-main" class="menu-main">
										<li><a href="javascript:;" class="current">Home</a></li>
										<li><a href="javascript:;">Ticket</a></li>
										<li><a href="javascript:;">How to Purchase</a></li>
										<li>
											<a href="javascript:;">Pages</a>
											<ul class="sub-menu">
												<li><a href="search.html">Search</a></li>
												<li><a href="login.html">Login</a></li>
												<li><a href="register.html">Register</a></li>
												<li><a href="blog-single.html">Blog Single</a></li>
												<li><a href="destination-single.html">Destination Single</a></li>
												<li><a href="booking-hotel-single.html">Booking Hotel Reservation</a></li>
												<li><a href="booking-hotel-payment.html">Booking Hotel Payment</a></li>
												<li><a href="booking-hotel-confirmation.html">Booking Hotel Confirmation</a></li>
												<li><a href="profile-hotel-availability.html">Profile Hotel Availability</a></li>
											</ul><!-- .sub-menu end -->
										</li>
									</ul><!-- #menu-main end -->
									<div class="menu-mobile-btn">
										<div class="hamburger hamburger--slider">
											<span class="hamburger-box">
												<span class="hamburger-inner"></span>
											</span>
										</div>
									</div><!-- .menu-mobile-btn end -->
									<a class="btn-header popup-btn-language-choice btn medium colorful hover-dark ml-30 ml-sm-20 move-top"
										href="javascript:;">EGP / AR</a>
								</div><!-- .hb-content end -->

							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .header-bar-wrap -->

			</div><!-- #header-bar-2 end -->

		</header><!-- #header end -->