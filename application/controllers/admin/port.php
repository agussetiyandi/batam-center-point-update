<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Port extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->m_admin->sesiku();
	}

	// View Port
	public function index()
	{
		$data['port']=$this->m_admin->tampil_port()->result();
		$this->load->view('admin/port/port',$data);
	}

	// Add Port Get Destination
	function add()
	{
		$data['destination']=$this->m_admin->tampil_destination();
		$this->load->view('admin/port/add',$data);
	}

	// Add Port
	function add_port()
	{
		$port_id = $this->input->post('port_id');
		$destination_id = $this->input->post('destination_id');
		$port_name = $this->input->post('port_name');
		$port_iso = $this->input->post('port_iso');

		$data = array(
			'port_id' => $port_id,
			'destination_id' => $destination_id,
			'port_name' => $port_name,
			'port_iso' => $port_iso,
		);
		$this->m_admin->add_port($data,'port');
		redirect('admin/port');

		// $data['airport'] = $this->input->post('airport');
		// $this->m_admin->add_airport($data,'airport');
		// redirect('admin/airport','refresh');
	}

	// Edit Port
	function edit($port_id)
	{
		$data['port'] = $this->m_admin->edit_port('port',$port_id)->result();
		$this->load->view('admin/port/edit',$data);
	}

	// Update Port
	function update($port_id){
		$port_name = $this->input->post('port_name');
		$port_iso = $this->input->post('port_iso');

		$data = array(
			'port_name' => $port_name,
			'port_iso' => $port_iso,
		);
		$this->db->set($data);
		$this->m_admin->update_port($port_id,$data);
		redirect('admin/port','refresh');
	}

	// Delete Port
	function del($port_id)
	{
		$this->m_admin->hapus_port($port_id);
		redirect('admin/port','refresh');
	}

}

/* End of file port.php */
/* Location: ./application/controllers/port.php */