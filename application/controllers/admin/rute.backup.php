<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rute extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_admin');
		$this->m_admin->sesiku();

	}

	// View Rute
	function index(){
		$arr_data_rute= array();
		$data_rute = $this->m_admin->tampil_rute();
		foreach($data_rute as $key) {
			$datasaturute = array();
			$port_from = $this->m_admin->tampil_port($key->rute_from)->row();
			$port_to = $this->m_admin->tampil_port($key->rute_to)->row();
			$datasaturute = array(

				'rute_id' => $key->rute_id,
				'rute_departure' => $key->rute_departure,
				'rute_arrival' => $key->rute_arrival,
				'port_from' => $port_from->destination_name,
				'rute_from' => $port_from->port_name,
				'port_iso_from' => $port_from->port_iso,
				'port_to' => $port_to->destination_name,
				'rute_to' => $port_to->port_name,
				'port_iso_to' => $port_to->port_iso,
				'rute_price' => $key->rute_price,
				'vessel' => $key->vessel,
				'ship_id' => $key->ship_id,
				'date_inserted' => $key->date_inserted,

			);
			array_push($arr_data_rute, $datasaturute);
		}
		$data['rute'] = $arr_data_rute;
		$this->load->view('admin/rute/rute',$data);
	}

	// Add
	function add(){
		$data['ship']=$this->m_admin->tampil_ship();
		$data['destination']=$this->m_admin->tampil_destination();
		// $data['rute'] = $this->m_admin->tampil_rute()->result();
		$this->load->view('admin/rute/add',$data);
	}

	// Add Rute
	function add_rute(){
		$rute_id = $this->input->post('rute_id');
		$ship_id = $this->input->post('ship_id');
		$rute_from = $this->input->post('rute_from');
		$rute_to = $this->input->post('rute_to');
		$rute_departure = $this->input->post('rute_departure');
		$rute_arrival = $this->input->post('rute_arrival');
		$rute_price = $this->input->post('rute_price');

		$data = array(
			'rute_id' => $rute_id,
			'ship_id' => $ship_id,
			'rute_from' => $rute_from,
			'rute_to' => $rute_to,
			'rute_departure' => $rute_departure,
			'rute_arrival' => $rute_arrival,
			'rute_price' => $rute_price,
		);
		$this->m_admin->add_rute($data,'rute');
		redirect('admin/rute');
	}

	// Update Rute
	function update_rute(){
		$rute_id = $this->input->post('id');
		$ship_id = $this->input->post('id_transportation');
		$rute_from = $this->input->post('rute_from');
		$rute_to = $this->input->post('rute_to');
		$rute_departure = $this->input->post('rute_departure');
		$rute_arrival = $this->input->post('rute_arrival');
		$rute_price = $this->input->post('rute_price');

		$data = array(
			'rute_id' => $rute_id,
			'ship_id' => $ship_id,
			'rute_from' => $rute_from,
			'rute_to' => $rute_to,
			'rute_departure' => $rute_departure,
			'rute_arrival' => $rute_arrival,
			'rute_price' => $rute_price,
		);
		$this->m_admin->update_rute($rute_id,$data);
		redirect('admin/rute');
	}

	// Delete Rute
	function del($rute_id){
		$this->m_admin->hapus_rute($rute_id);
		$this->session->set_flashdata('notif','<div class="alert alert-success" role="alert"> Data Successfully Changed <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('admin/rute','refresh');
	}

	// Edit
	function edit($rute_id){
		$data['ship']=$this->m_admin->tampil_ship();
		$data['destination']=$this->m_admin->tampil_destination();
		$arr_data_rute= array();
		$data_rute = $this->m_admin->tampil_rute($id);
		foreach($data_rute as $key) {
			$datasaturute = array();
			$port_from = $this->m_admin->tampil_port($key->rute_from)->row();
			$port_to = $this->m_admin->tampil_port($key->rute_to)->row();
			$datakotafrom = $this->m_admin->get_port($port_from->idkota);
			$optionfrom = '<option value="">Pilih Bandara</option>';
			foreach($datakotafrom->result() as $keyy){
				$optionfrom .= '<option value="'.$keyy->id.'" '.($keyy->id==$key->rute_from?"selected":"").'>'.$keyy->name.' ('.$keyy->iso.')</option>';
			}
			$datakotato = $this->m_admin->get_airport($airport_to->idkota);
			$optionto = '<option value="">Pilih Bandara</option>';
			foreach($datakotato->result() as $keyy){
				$optionto .= '<option value="'.$keyy->id.'" '.($keyy->id==$key->rute_to?"selected":"").'>'.$keyy->name.' ('.$keyy->iso.')</option>';
			}
			$datasaturute = array(
				'id' => $key->id,
				'idkotafrom' => $airport_from->idkota,
				'idkotato' => $airport_to->idkota,
				'optionbandarafrom' => $optionfrom,
				'optionbandarato' => $optionto,
				'depart_at' => $key->depart_at,
				'arrival' => $key->arrival,
				'mangkat_from' => $airport_from->destination,
				'rute_from' => $airport_from->name,
				'iso_from' => $airport_from->iso,
				'mangkat_to' => $airport_to->destination,
				'rute_to' => $airport_to->name,
				'iso_to' => $airport_to->iso,
				'price' => $key->price,
				'vessel' => $key->vessel,
				'idmaskapai' => $key->id_transportation,
				'creat_date' => $key->creat_date,
			);
			array_push($arr_data_rute, $datasaturute);
		}
		$data['rute'] = $arr_data_rute;
		$this->load->view('admin/rute/edit',$data);
	}

	function getairport($id){
		$data = $this->m_admin->get_airport($id);
		$option = '<option value="">Pilih Bandara</option>';
		foreach($data->result() as $key){
			$option .= '<option value="'.$key->id.'">'.$key->name.' ('.$key->iso.')</option>';
		}
		echo $option;
	}

}